package TimeStopper;

import cpw.mods.fml.common.FMLLog;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.SidedProxy;
import cpw.mods.fml.common.event.FMLInitializationEvent;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
import cpw.mods.fml.common.registry.EntityRegistry;
import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.common.registry.LanguageRegistry;

import java.util.logging.Level;

import net.minecraft.block.Block;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;

@Mod(modid="TimeStopper", name="TimeStopper Mod", version="1.1.0")
public class ModTimeStopper
{
	@SidedProxy(clientSide="TimeStopper.ClientProxy", serverSide="TimeStopper.CommonProxy")
	public static CommonProxy proxy;
	@Mod.Instance("TimeStopper")
	public static ModTimeStopper instance;
	public static Item timeStopper;
	private int timeStopperID;
	private int stopperEntityID;
	public static float clientYaw = 0.0F;
	public static Item timeStopper2;
	public static boolean timeStopped = false;

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		timeStopper = new ItemTimeStopper("TimeStopper");
		timeStopper.setCreativeTab(CreativeTabs.tabMisc);
		Block block = new BlockWaterDetector("blockWaterDetector");

		EntityRegistry.registerGlobalEntityID(EntityTimeStopper.class, "TimeStopper", this.stopperEntityID);
		EntityRegistry.registerModEntity(EntityTimeStopper.class, "TimeStopper", 0, this, 400, 1, true);
		MinecraftForge.EVENT_BUS.register(new TimeEvent());
		if (event.getSide().isClient())
		{
			MinecraftForge.EVENT_BUS.register(new EventHookAttackEntity());
		}

		proxy.registerRenderers();

		GameRegistry.addRecipe(new ItemStack(timeStopper, 1), new Object[] { " G ", "GTG", " G ", Character.valueOf('T'), new ItemStack(Items.clock, 1), Character.valueOf('G'), new ItemStack(Items.glowstone_dust, 4) });
	}
}

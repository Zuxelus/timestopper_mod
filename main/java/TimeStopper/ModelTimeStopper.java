package TimeStopper;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.model.ModelRenderer;
import net.minecraft.entity.Entity;

public class ModelTimeStopper extends ModelBase
{
	public ModelRenderer timeStopper;

	public ModelTimeStopper()
	{
		this.timeStopper = new ModelRenderer(this, 0, 0);
		this.timeStopper.addBox(-8.0F, -8.0F, -0.5F, 16, 16, 1);
	}

	@Override
	public void render(Entity entity, float f, float f1, float f2, float f3, float f4, float f5)
	{
		this.timeStopper.render(f5);
	}
}

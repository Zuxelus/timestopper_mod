package TimeStopper;

import java.util.HashMap;
import java.util.List;
import java.util.Random;

import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityCreature;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.item.EntityTNTPrimed;
import net.minecraft.entity.monster.EntityCreeper;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.projectile.EntityArrow;
import net.minecraft.item.Item;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.DamageSource;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class EntityTimeStopper extends Entity
{
	private double targetX;
	private double targetZ;
	private double targetY;
	private int despawnTimer;
	private int despawnTimer2;
	private boolean shatterOrDrop;
	private long worldTime;
	// Lists
	public HashMap entityPos;
	public HashMap entityVelocity;
	public HashMap livingData;
	private HashMap targetList;
	private HashMap ignitedList;
	private HashMap healthList;
	private HashMap TNTList;
	private HashMap mobTime;
	private HashMap groundList;
	public HashMap totalDamageList;

	public EntityTimeStopper(World par1World)
	{
		super(par1World);
		setSize(0.25F, 0.25F);
		this.worldTime = par1World.getWorldInfo().getWorldTime();

		this.entityPos = new HashMap();
		this.entityVelocity = new HashMap();
		this.livingData = new HashMap();
		this.targetList = new HashMap();
		this.ignitedList = new HashMap();
		this.healthList = new HashMap();
		this.TNTList = new HashMap();
		this.mobTime = new HashMap();
		this.groundList = new HashMap();
		this.totalDamageList = new HashMap();
	}

	@Override
	protected void entityInit() {}

	@SideOnly(Side.CLIENT)
	@Override
	public boolean isInRangeToRenderDist(double par1)
	{
		double d = this.boundingBox.getAverageEdgeLength() * 4.0D;
		d *= 64.0D;
		return par1 < d * d;
	}

	public EntityTimeStopper(World par1World, double par2, double par4, double par6)
	{
		super(par1World);
		this.despawnTimer = 0;
		this.despawnTimer2 = 0;
		setSize(0.25F, 0.25F);
		setPosition(par2, par4, par6);
		this.yOffset = 0.0F;
		this.entityPos = new HashMap();
		this.entityVelocity = new HashMap();
		this.livingData = new HashMap();
		this.targetList = new HashMap();
		this.ignitedList = new HashMap();
		this.healthList = new HashMap();
		this.TNTList = new HashMap();
		this.mobTime = new HashMap();
		this.groundList = new HashMap();
		this.worldTime = par1World.getWorldInfo().getWorldTime();
	}

	@Override
	public void setPositionAndRotation(double par1, double par3, double par5, float par7, float par8)
	{
		setPosition(par1, par3, par5);
		setRotation(par7, par8);
	}

	@Override
	public void setPositionAndRotation2(double par1, double par3, double par5, float par7, float par8, int par9)
	{
		setPositionAndRotation(par1, par3, par5, par7, par8);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public void setVelocity(double par1, double par3, double par5)
	{
		this.motionX = par1;
		this.motionY = par3;
		this.motionZ = par5;
		if (this.prevRotationPitch == 0.0F && this.prevRotationYaw == 0.0F)
		{
			float f = MathHelper.sqrt_double(par1 * par1 + par5 * par5);
			this.prevRotationYaw = (this.rotationYaw = (float)(Math.atan2(par1, par5) * 180.0D / Math.PI));
			this.prevRotationPitch = (this.rotationPitch = 0.0F);
		}
	}

	@Override
	public void onUpdate()
	{
		this.lastTickPosX = this.posX;
		this.lastTickPosY = this.posY;
		this.lastTickPosZ = this.posZ;
		super.onUpdate();
		this.posX += this.motionX;
		this.posY += this.motionY;
		this.posZ += this.motionZ;
		this.motionX *= 0.7D;
		this.motionY *= 0.7D;
		this.motionZ *= 0.7D;
		if (!this.worldObj.isRemote)
		{
			this.rotationYaw = ((float)Math.exp(this.despawnTimer / 27.0D) * 40.0F);
		}
		this.worldObj.getWorldInfo().setWorldTime(this.worldTime);
		for (int i = 0; i < this.worldObj.loadedEntityList.size(); i++)
		{
			Entity entity = (Entity)this.worldObj.loadedEntityList.get(i);
			if (!entity.getClass().getSimpleName().equals("EntityItemFrame") && !entity.getClass().getSimpleName().equals("EntityPainting"))
			{
				int id = entity.getEntityId();
				double[] tempPos = { entity.posX, entity.posY, entity.posZ, entity.rotationPitch, entity.rotationYaw, entity.fallDistance };

				int ticks = entity.ticksExisted;
				double[] tempVelocity = { entity.motionX, entity.motionY, entity.motionZ };
				if (!(entity instanceof EntityTimeStopper) && !(entity instanceof EntityPlayer))
				{
					if (!this.entityPos.containsKey(Integer.valueOf(id)))
					{
						this.entityPos.put(Integer.valueOf(id), tempPos);
					}
					tempPos = (double[])this.entityPos.get(Integer.valueOf(id));
					if (!this.mobTime.containsKey(Integer.valueOf(id)))
					{
						this.mobTime.put(Integer.valueOf(id), Integer.valueOf(ticks));
					}
					if (!this.entityVelocity.containsKey(Integer.valueOf(id)))
					{
						this.entityVelocity.put(Integer.valueOf(id), tempVelocity);
					}
					tempVelocity = (double[])this.entityVelocity.get(Integer.valueOf(id));
					if (entity instanceof EntityArrow)
					{
						boolean inGround = false;
						if (entity.motionY == 0.0D)
						{
							inGround = true;
							if (!this.groundList.containsKey(Integer.valueOf(id)))
							{
								this.groundList.put(Integer.valueOf(id), Boolean.valueOf(inGround));
								if (entity.ticksExisted > 5)
								{
									tempVelocity[0] *= this.rand.nextFloat() * 0.2F;
									tempVelocity[1] *= this.rand.nextFloat() * 0.2F;
									tempVelocity[2] *= this.rand.nextFloat() * 0.2F;
								}
							}
						}
					}
					entity.ticksExisted = ((Integer)this.mobTime.get(Integer.valueOf(id))).intValue();
					entity.lastTickPosX = tempPos[0];
					entity.lastTickPosY = tempPos[1];
					entity.lastTickPosZ = tempPos[2];
					entity.prevPosX = tempPos[0];
					entity.prevPosY = tempPos[1];
					entity.prevPosZ = tempPos[2];
					entity.setPosition(tempPos[0], tempPos[1], tempPos[2]);
					entity.prevRotationPitch = ((float)tempPos[3]);
					entity.prevRotationYaw = ((float)tempPos[4]);
					entity.rotationPitch = ((float)tempPos[3]);
					entity.rotationYaw = ((float)tempPos[4]);
					entity.fallDistance = ((float)tempPos[5]);
					//entity.setVelocity(0.0D, 0.0D, 0.0D);
					entity.motionX = 0.0D;
					entity.motionY = 0.0D;
					entity.motionZ = 0.0D;
					if (entity instanceof EntityLiving)
					{
						EntityLiving el = (EntityLiving)entity;
						double[] temp = { el.rotationYawHead, el.renderYawOffset, el.attackTime };
						if (!this.livingData.containsKey(Integer.valueOf(id)))
						{
							this.livingData.put(Integer.valueOf(id), temp);
						}
						temp = (double[])this.livingData.get(Integer.valueOf(id));
						el.prevRotationYawHead = ((float)temp[0]);
						el.prevRenderYawOffset = ((float)temp[1]);
						el.rotationYawHead = ((float)temp[0]);
						el.renderYawOffset = ((float)temp[1]);
						el.attackTime = ((int)temp[2]);
						float[] health = { el.getHealth(), el.getHealth() };
						if (!this.healthList.containsKey(Integer.valueOf(id)))
						{
							this.healthList.put(Integer.valueOf(id), health);
						}
						health = (float[])this.healthList.get(Integer.valueOf(id));
						health[1] -= health[0] - el.getHealth();

						el.setHealth(health[0]);

						entity = el;
					}
					if (entity instanceof EntityCreature)
					{
						EntityCreature ec = (EntityCreature)entity;
						EntityLivingBase target = ec.getAttackTarget();
						if (!this.targetList.containsKey(Integer.valueOf(id)))
						{
							this.targetList.put(Integer.valueOf(id), target);
						}
						ec.setAttackTarget(null);
						entity = ec;
					}
					if (entity instanceof EntityCreeper)
					{
						EntityCreeper ec = (EntityCreeper)entity;
						int time = ec.getCreeperState();
						if (!this.ignitedList.containsKey(Integer.valueOf(id)))
						{
							this.ignitedList.put(Integer.valueOf(id), Integer.valueOf(time));
						}
						time = ((Integer)this.ignitedList.get(Integer.valueOf(id))).intValue();
						ec.setCreeperState(0);
						entity = ec;
					}
					if (entity instanceof EntityTNTPrimed)
					{
						EntityTNTPrimed ec = (EntityTNTPrimed)entity;
						int time = ec.fuse;
						if (!this.TNTList.containsKey(Integer.valueOf(id)))
						{
							this.TNTList.put(Integer.valueOf(id), Integer.valueOf(time));
						}
						time = ((Integer)this.TNTList.get(Integer.valueOf(id))).intValue();
						ec.fuse = time;
						entity = ec;
					}
				}
			}
		}
		setPosition(this.posX, this.posY, this.posZ);
		if (!this.worldObj.isRemote)
		{
			this.despawnTimer += 1;
		}
		else
		{
			this.despawnTimer2 += 1;
		}
		if (!this.worldObj.isRemote)
		{
			if (this.despawnTimer > 150 || this.despawnTimer2 > 150)
			{
				if (!this.worldObj.isRemote)
				{
					setDead();
				}
				for (int i = 0; i < this.worldObj.loadedEntityList.size(); i++)
				{
					Entity entity = (Entity)this.worldObj.loadedEntityList.get(i);
					if (!entity.getClass().getSimpleName().equals("EntityItemFrame") && !entity.getClass().getSimpleName().equals("EntityPainting"))
					{
						int id = entity.getEntityId();
						if (!(entity instanceof EntityTimeStopper) && !(entity instanceof EntityPlayer))
						{
							double[] tempVelocity = new double[3];
							if (!this.entityVelocity.containsKey(Integer.valueOf(id)))
							{
								this.entityVelocity.put(Integer.valueOf(id), tempVelocity);
							}
							tempVelocity = (double[])this.entityVelocity.get(Integer.valueOf(id));
							//entity.setVelocity(tempVelocity[0], tempVelocity[1], tempVelocity[2]);
							entity.motionX = tempVelocity[0];
							entity.motionY = tempVelocity[1];
							entity.motionZ = tempVelocity[2];
							if (entity instanceof EntityCreature)
							{
								EntityCreature ec = (EntityCreature)entity;
								EntityLivingBase target = ec.getAttackTarget();
								if (!this.targetList.containsKey(Integer.valueOf(id)))
								{
									this.targetList.put(Integer.valueOf(id), target);
								}
								target = (EntityLivingBase)this.targetList.get(Integer.valueOf(id));
								ec.setAttackTarget(target);
								entity = ec;
							}
							if (entity instanceof EntityCreeper)
							{
								EntityCreeper ec = (EntityCreeper)entity;
								int time = ec.getCreeperState();
								if (!this.ignitedList.containsKey(Integer.valueOf(id)))
								{
									this.ignitedList.put(Integer.valueOf(id), Integer.valueOf(time));
								}
								time = ((Integer)this.ignitedList.get(Integer.valueOf(id))).intValue();
								ec.setCreeperState(time);
								entity = ec;
							}
							if (entity instanceof EntityLiving)
							{
								EntityLiving el = (EntityLiving)entity;
								float[] health = new float[2];
								if (this.totalDamageList != null && this.totalDamageList.containsKey(Integer.valueOf(id)))
								{
									Object[] damage = (Object[])this.totalDamageList.get(Integer.valueOf(id));
									DamageSource source = (DamageSource)damage[0];
									float totalDamage = ((Float)damage[1]).floatValue();
									el.attackEntityFrom(source, totalDamage);
								}
							}
						}
					}
				}
				if (!this.shatterOrDrop)
				{
					crashEffect(this.posX, this.posY, this.posZ);
				}
			}
		}
	}

	public void crashEffect(double par3, double par4, double par5)
	{
		Random random = this.worldObj.rand;
		double d1 = par3;
		double d5 = par4;
		double d9 = par5;

		String s1 = "iconcrack_" + Item.getIdFromItem(ModTimeStopper.timeStopper);
		for (int j1 = 0; j1 < 8; j1++)
		{
			this.worldObj.spawnParticle(s1, d1, d5, d9, random.nextGaussian() * 0.15D, random.nextDouble() * 0.15D, random.nextGaussian() * 0.15D);
		}
		this.worldObj.playSoundEffect(par3 + 0.5D, par4 + 0.5D, par5 + 0.5D, "random.glass", 2.0F, this.worldObj.rand.nextFloat() * 0.1F + 1.9F);
	}

	@Override
	public void writeEntityToNBT(NBTTagCompound nbttagcompound) {}

	@Override
	public void readEntityFromNBT(NBTTagCompound nbttagcompound) {}

	@Override
	public void onCollideWithPlayer(EntityPlayer entityplayer) {}

	@SideOnly(Side.CLIENT)
	@Override
	public float getShadowSize()
	{
		return 0.0F;
	}

	@Override
	public float getBrightness(float par1)
	{
		return 1.0F;
	}

	@SideOnly(Side.CLIENT)
	@Override
	public int getBrightnessForRender(float par1)
	{
		return 15728880;
	}

	@Override
	public boolean canAttackWithItem()
	{
		return false;
	}
}

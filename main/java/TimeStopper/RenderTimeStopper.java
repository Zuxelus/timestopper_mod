package TimeStopper;

import net.minecraft.client.model.ModelBase;
import net.minecraft.client.renderer.entity.Render;
import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;

import org.lwjgl.opengl.GL11;

public class RenderTimeStopper extends Render
{
	protected ModelBase baseTimeStopper;
	private static final ResourceLocation texture = new ResourceLocation("TimeStopper:textures/items/TimeStopperTex.png");

	public RenderTimeStopper()
	{
		this.baseTimeStopper = new ModelTimeStopper();
	}

	public void renderTheWolrd(EntityTimeStopper par1EntityTheWorld, double par2, double par4, double par6, float par8, float par9)
	{
		bindEntityTexture(par1EntityTheWorld);
		GL11.glPushMatrix();
		GL11.glTranslatef((float)par2, (float)par4, (float)par6);
		GL11.glRotatef(par1EntityTheWorld.prevRotationYaw + (par1EntityTheWorld.rotationYaw - par1EntityTheWorld.prevRotationYaw) * par9 - 90.0F, 0.0F, 1.0F, 0.0F);
		GL11.glRotatef(par1EntityTheWorld.prevRotationPitch + (par1EntityTheWorld.rotationPitch - par1EntityTheWorld.prevRotationPitch) * par9, 0.0F, 0.0F, 1.0F);
		GL11.glScalef(0.3F, 0.3F, 0.3F);

		this.baseTimeStopper.render(par1EntityTheWorld, 0.0F, 0.0F, -0.1F, 0.0F, 0.0F, 0.0625F);
		GL11.glPopMatrix();
	}

	@Override
	public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9)
	{
		renderTheWolrd((EntityTimeStopper)par1Entity, par2, par4, par6, par8, par9);
	}

	@Override
	protected ResourceLocation getEntityTexture(Entity entity)
	{
		return texture;
	}
}

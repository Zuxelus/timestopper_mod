package TimeStopper;

import java.util.ArrayList;
import java.util.List;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.entity.Entity;
import net.minecraftforge.event.entity.living.LivingDropsEvent;

public class TimeEvent
{
	List entitylist = new ArrayList();

	@SubscribeEvent
	public void onLivingDrops(LivingDropsEvent event)
	{
		if (!event.entityLiving.worldObj.isRemote)
		{
			List list = event.entityLiving.worldObj.loadedEntityList;
			if (list == null)
			{
				return;
			}
			for (int i = 0; i < list.size(); i++)
			{
				Entity entity = (Entity)list.get(i);
				if (entity instanceof EntityTimeStopper)
				{
					if (this.entitylist.contains(Integer.valueOf(event.entityLiving.getEntityId())))
					{
						event.drops.clear();
					}
					this.entitylist.add(Integer.valueOf(event.entityLiving.getEntityId()));
					return;
				}
			}
			if (!this.entitylist.isEmpty())
			{
				this.entitylist.clear();
			}
		}
	}
}

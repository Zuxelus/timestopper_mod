package TimeStopper;

import java.util.List;
import java.util.Random;

import cpw.mods.fml.common.registry.GameRegistry;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.MathHelper;
import net.minecraft.world.World;

public class ItemTimeStopper extends Item
{
	public ItemTimeStopper(String name)
	{
		super();
		this.maxStackSize = 1;
		setUnlocalizedName(name);
		setTextureName("TimeStopper:TimeStopperIcon");
		GameRegistry.registerItem(this, name);
	}

	@Override
	public ItemStack onItemRightClick(ItemStack par1ItemStack, World par2World, EntityPlayer par3EntityPlayer)
	{
		if (par3EntityPlayer.ridingEntity != null)
		{
			return par1ItemStack;
		}
		for (int i = 0; i < par2World.loadedEntityList.size(); i++)
		{
			Entity entity = (Entity)par2World.loadedEntityList.get(i);
			if (entity instanceof EntityTimeStopper)
			{
				return par1ItemStack;
			}
		}
		par1ItemStack.stackSize -= 1;
		par2World.playSoundAtEntity(par3EntityPlayer, "random.bow", 0.5F, 0.4F / (itemRand.nextFloat() * 0.4F + 0.8F));
		if (!par2World.isRemote)
		{
			EntityTimeStopper theworld = new EntityTimeStopper(par2World, par3EntityPlayer.posX, par3EntityPlayer.posY + 1.62D - par3EntityPlayer.yOffset, par3EntityPlayer.posZ);
			//theworld.setVelocity(-1.0D * Math.sin(par3EntityPlayer.rotationYaw / 180.0F * 3.141592653589793D), -1.0D * Math.sin(par3EntityPlayer.rotationPitch / 180.0F * 3.141592653589793D), 1.0D * Math.cos(par3EntityPlayer.rotationYaw / 180.0F * 3.141592653589793D));
			theworld.motionX = -1.0D * Math.sin(par3EntityPlayer.rotationYaw / 180.0F * 3.141592653589793D);
			theworld.motionY = -1.0D * Math.sin(par3EntityPlayer.rotationPitch / 180.0F * 3.141592653589793D);
			theworld.motionZ = 1.0D * Math.cos(par3EntityPlayer.rotationYaw / 180.0F * 3.141592653589793D);
			if (theworld.prevRotationPitch == 0.0F && theworld.prevRotationYaw == 0.0F)
			{
				theworld.prevRotationYaw = (theworld.rotationYaw = (float)(Math.atan2(-1.0D * Math.sin(par3EntityPlayer.rotationYaw / 180.0F * 3.141592653589793D), Math.cos(par3EntityPlayer.rotationYaw / 180.0F * 3.141592653589793D)) * 180.0D / Math.PI));
				theworld.prevRotationPitch = (theworld.rotationPitch = 0.0F);
			}
			par2World.spawnEntityInWorld(theworld);
		}
		return par1ItemStack;
	}
}

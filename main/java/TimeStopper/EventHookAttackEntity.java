package TimeStopper;

import java.util.ArrayList;
import java.util.HashMap;

import cpw.mods.fml.common.eventhandler.SubscribeEvent;
import net.minecraft.client.Minecraft;
import net.minecraft.client.particle.EntityCrit2FX;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.living.LivingAttackEvent;

public class EventHookAttackEntity
{
	@SubscribeEvent
	public void onLivingAttack(LivingAttackEvent event)
	{
		World world = event.entityLiving.worldObj;
		ArrayList entitylist = (ArrayList)world.loadedEntityList;
		if (entitylist == null)
		{
			return;
		}
		if (entitylist.isEmpty())
		{
			return;
		}
		int i = 0;
		Entity entity = null;
		while (i < entitylist.size())
		{
			entity = (Entity)entitylist.get(i);
			if (entity instanceof EntityTimeStopper)
			{
				break;
			}
			if (i == entitylist.size() - 1)
			{
				return;
			}
			i++;
		}
		if (event.entityLiving instanceof EntityPlayer)
		{
			return;
		}
		EntityTimeStopper timeStopper = (EntityTimeStopper)entity;
		int entityId = event.entityLiving.getEntityId();
		float currentDamage = event.ammount;
		if (timeStopper.isDead)
		{
			return;
		}
		if (timeStopper.totalDamageList == null)
		{
			timeStopper.totalDamageList = new HashMap();
		}
		if (timeStopper.totalDamageList.containsKey(Integer.valueOf(entityId)))
		{
			float totalDamage = ((Float)((Object[])(Object[])timeStopper.totalDamageList.get(Integer.valueOf(entityId)))[1]).floatValue() + currentDamage;

			Object[] damage = { event.source, Float.valueOf(totalDamage) };
			timeStopper.totalDamageList.put(Integer.valueOf(entityId), damage);
		}
		else
		{
			Object[] damage = { event.source, Float.valueOf(currentDamage) };
			timeStopper.totalDamageList.put(Integer.valueOf(entityId), damage);
		}
		EntityCrit2FX entitycrit2fx = new EntityCrit2FX(world, event.entityLiving);
		entitycrit2fx.setRBGColorF(0.0F, 1.0F, 0.0F);
		entitycrit2fx.setAlphaF(0.5F);
		Minecraft.getMinecraft().effectRenderer.addEffect(entitycrit2fx);

		event.setCanceled(true);
	}
}

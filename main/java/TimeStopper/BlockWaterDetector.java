package TimeStopper;

import java.util.Random;

import cpw.mods.fml.common.registry.GameRegistry;
import cpw.mods.fml.relauncher.Side;
import cpw.mods.fml.relauncher.SideOnly;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.texture.IIconRegister;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.util.IIcon;
import net.minecraft.world.IBlockAccess;
import net.minecraft.world.World;

public class BlockWaterDetector extends Block
{
	private IIcon[] textures = new IIcon[3];

	public BlockWaterDetector(String name)
	{
		super(Material.rock);
		setBlockName(name);
		setHardness(2.0F);
		setStepSound(Block.soundTypeStone);
		setCreativeTab(CreativeTabs.tabRedstone);
		GameRegistry.registerBlock(this, name);
	}

	@SideOnly(Side.CLIENT)
	@Override
	public IIcon getIcon(int side, int metadata)
	{
		if (side > 1)
		{
			return textures[2];
		}
		return textures[side];
	}

	@Override
	public void registerBlockIcons(IIconRegister iconRegister)
	{
		this.textures[0] = iconRegister.registerIcon("timestopper:water_detector_block_base");
		this.textures[1] = iconRegister.registerIcon("timestopper:water_detector_block_top");
		this.textures[2] = iconRegister.registerIcon("timestopper:water_detector_block_side");
	}

	@Override
	public int isProvidingWeakPower(IBlockAccess iblockaccess, int x, int y, int z, int side)
	{
		return power(iblockaccess, x ,y ,z);
	}

	private int power(IBlockAccess world, int x, int y, int z)
	{
		int power = 0;
		power += addPower(world.getBlock(x - 1, y, z));
		power += addPower(world.getBlock(x + 1, y, z));
		power += addPower(world.getBlock(x, y - 1, z));
		power += addPower(world.getBlock(x, y + 1, z));
		power += addPower(world.getBlock(x, y, z - 1));
		power += addPower(world.getBlock(x, y, z + 1));

		return power;
	}

	private int addPower(Block block)
	{
		if (block == Blocks.water)
		{
			return 2;
		}
		if (block == Blocks.flowing_water)
		{
			return 1;
		}
		return 0;
	}

	@Override
	public int isProvidingStrongPower(IBlockAccess iblockaccess, int x, int y, int z, int side)
	{
		return this.isProvidingWeakPower(iblockaccess, x, y, z, side);
	}

	@Override
	public boolean canProvidePower()
	{
		return true;
	}

	@Override
	public void onBlockAdded(World world, int x, int y, int z)
	{
		super.onBlockAdded(world, x, y, z);
		world.notifyBlocksOfNeighborChange(x, y - 1, z, this);
		world.notifyBlocksOfNeighborChange(x, y + 1, z, this);
		world.notifyBlocksOfNeighborChange(x - 1, y, z, this);
		world.notifyBlocksOfNeighborChange(x + 1, y, z, this);
		world.notifyBlocksOfNeighborChange(x, y, z - 1, this);
		world.notifyBlocksOfNeighborChange(x, y, z + 1, this);
	}

	@Override
	public void breakBlock(World world, int x, int y, int z, Block block, int p_149749_6_)
	{
		super.breakBlock(world, x, y, z, block, p_149749_6_);
		world.notifyBlocksOfNeighborChange(x, y - 1, z, this);
		world.notifyBlocksOfNeighborChange(x, y + 1, z, this);
		world.notifyBlocksOfNeighborChange(x - 1, y, z, this);
		world.notifyBlocksOfNeighborChange(x + 1, y, z, this);
		world.notifyBlocksOfNeighborChange(x, y, z - 1, this);
		world.notifyBlocksOfNeighborChange(x, y, z + 1, this);
	}

	@Override
	public void onNeighborBlockChange(World world, int x, int y, int z, Block neighborBlock)
	{
		super.onNeighborBlockChange(world, x, y, z, neighborBlock);
		if (neighborBlock == Blocks.water || neighborBlock == Blocks.flowing_water || neighborBlock == Blocks.air)
		{
			world.notifyBlocksOfNeighborChange(x, y + 1, z, this);
			world.notifyBlocksOfNeighborChange(x - 1, y, z, this);
			world.notifyBlocksOfNeighborChange(x + 1, y, z, this);
			world.notifyBlocksOfNeighborChange(x, y, z - 1, this);
			world.notifyBlocksOfNeighborChange(x, y, z + 1, this);
		} 	
	}
}